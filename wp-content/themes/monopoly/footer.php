
		    </div>
		    <div class="wrap2">
		    	<?php wp_footer();?>
			</div>
		</div>
		<div class="modal modal-my fade" id="contactForm">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header box-style">
            			<div data-dismiss="modal" class="modal-close"><div></div></div>

						<div class="box-style__header">
							<div class="box-style__heading">
								Оставить заявку
							</div>
						</div>
					</div>
					<div class="modal-body">
						<?= do_shortcode('[contact-form-7 id="46" title="Contact form 1"]'); ?>
					</div>
				</div>
			</div>
		</div>
		<div id="messageModal" class="modal modal-my modal-my-xs fade" role="dialog">
		    <div class="modal-dialog" role="document">
		        <div class="modal-content">
		            <div class="modal-header box-style">
		                <div data-dismiss="modal" class="modal-close"><div></div></div>
		                <div class="box-style__header">
		                    <div class="box-style__heading">
		                        Уведомление
		                    </div>
		                </div>
		            </div>
		            <div class="modal-body">
		                <div class="message-success">
		                    Ваша заявка успешно отправлена!
		                </div>
		            </div>
		        </div>
		    </div>
		</div>
	    <script type="text/javascript" >
	    	function metrika() {
		       (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
		       m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
		       (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

		       ym(68298607, "init", {
		            clickmap:true,
		            trackLinks:true,
		            accurateTrackBounce:true,
		            webvisor:true
		       });
			   !function(){var t=document.createElement("script");t.type="text/javascript",t.async=!0,t.src="https://vk.com/js/api/openapi.js?168",t.onload=function(){VK.Retargeting.Init("VK-RTRG-534781-3CrtS"),VK.Retargeting.Hit()},document.head.appendChild(t)}();
	    	}
	    	setTimeout(metrika, 5000);
	    </script>
	    <noscript><div><img src="https://mc.yandex.ru/watch/68298607" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
        <noscript><img src="https://vk.com/rtrg?p=VK-RTRG-534781-3CrtS" style="position:fixed; left:-999px;" alt=""/></noscript>

        <style>
        	.hidden{
        		display: none;
        	}
        </style>
  </body>
</html>