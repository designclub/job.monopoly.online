$(document).ready(function() {
	$("a[href^='#']").on('click', function(){
		var href = $(this).attr('href');
		var top = $(href).offset().top;
	    $('body,html').animate({
	        scrollTop: top + 'px'
	    }, 400);
	    return false;
	});
});

// @prepros-append "./lib/jquery.inputmask.js"