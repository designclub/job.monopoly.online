<?php 
	/*
	 * Подключаем стили и скрипты
	*/
	function monopoly_mystyle() {
		wp_enqueue_style('bootstrap', get_stylesheet_directory_uri() . '/css/bootstrap.min.css');
		wp_enqueue_style('styles', get_stylesheet_directory_uri() . '/css/index.css?fhsdffds');
	}
	add_action('wp_enqueue_scripts', 'monopoly_mystyle');

	function monopoly_myscript(){
		// wp_enqueue_script('jquery', 'https://code.jquery.com/jquery-3.5.1.min.js', '', '', false);
		wp_enqueue_script('bootstrap', get_stylesheet_directory_uri() . '/js/bootstrap.min.js', '', '', true);
		wp_enqueue_script('index', get_stylesheet_directory_uri() . '/js/index.js', '', '', true);
	}

	add_action('wp_enqueue_scripts', 'monopoly_myscript');

	function my_update_jquery () {
		if ( !is_admin() ) { 
		   wp_deregister_script('jquery-core');
		   wp_register_script('jquery-core', 'https://code.jquery.com/jquery-1.12.4.min.js', false, false, true);
		   wp_enqueue_script('jquery');
		}
	}
	add_action('wp_enqueue_scripts', 'my_update_jquery');

	/***********************************************************/
	/***********************************************************/

	// Добавляем Лого
	add_theme_support('custom-logo');

	// Добавляем поддержку изображений в постах
	add_theme_support('post-thumbnails');

	// Добавляем виджеты
	add_theme_support('widgets');

	// Добавляем меню
	register_nav_menus([
		'top'  => 'Верхнее меню', // идентификатор меню и название
		'footer'  => 'Нижнее меню', // идентификатор меню и название
	]);


	/***********************************************************/
	/***********************************************************/

	/**
	* Добавляет блок для ввода контактных данных
	*/
	// function mytheme_customize_register( $wp_customize ) {
	// 	/*
	// 	Добавляем секцию в настройки темы
	// 	*/
	// 	$wp_customize->add_section(
	// 		'data_site_section',
	// 		array(
	// 			'title' => 'Контактные данные',
	// 			'capability' => 'edit_theme_options',
	// 			'description' => "Тут можно указать контактные данные"
	// 		)
	// 	);
	// 	/*
	// 	Добавляем поле Адрес
	// 	*/
	// 	$wp_customize->add_setting(
	// 		'site_adress',
	// 		array(
	// 			'default' => '',
	// 			'type' => 'option'
	// 		)
	// 	);
	// 	$wp_customize->add_control(
	// 		'site_adress_control',
	// 		array(
	// 			'type' => 'text',
	// 			'label' => "Адрес:",
	// 			'section' => 'data_site_section',
	// 			'settings' => 'site_adress'
	// 		)
	// 	);
	// 	/*
	// 	Добавляем поле Телефон
	// 	*/
	// 	$wp_customize->add_setting(
	// 		'site_phone',
	// 		array(
	// 			'default' => '',
	// 			'type' => 'option'
	// 		)
	// 	);
	// 	$wp_customize->add_control(
	// 		'site_phone_control',
	// 		array(
	// 			'type' => 'text',
	// 			'label' => "Телефон:",
	// 			'section' => 'data_site_section',
	// 			'settings' => 'site_phone'
	// 		)
	// 	);
	// 	/*
	// 	Добавляем поле E-mail
	// 	*/
	// 	$wp_customize->add_setting(
	// 		'site_email',
	// 		array(
	// 			'default' => '',
	// 			'type' => 'option'
	// 		)
	// 	);
	// 	$wp_customize->add_control(
	// 		'site_email_control',
	// 		array(
	// 			'type' => 'text',
	// 			'label' => "E-mail:",
	// 			'section' => 'data_site_section',
	// 			'settings' => 'site_email'
	// 		)
	// 	);
	// }
	// add_action( 'customize_register', 'mytheme_customize_register' );

	require get_template_directory() . '/include/options-panel.php';

	require get_template_directory() . '/include/seo.php';
	Kama_SEO_Tags::init();

	add_filter('wpcf7_autop_or_not', '__return_false');
	
	add_action( 'wp_footer', 'mycustom_wp_footer' );
 
	function mycustom_wp_footer() { ?>
		<script type="text/javascript">
			document.addEventListener( 'wpcf7mailsent', function( event ) {
				yaCounter68298607.reachGoal('Form');
				VK.Goal('lead');
				$("#contactForm").modal('hide');
				$("#messageModal").modal('show');
				setTimeout(function(){
					$("#messageModal").modal('hide');
				}, 4000);
			}, false );
		</script>
		<?php 
	}

	// Вызываем функцию для перехвата данных
	add_action( 'wpcf7_mail_sent', 'monopoly_wpcf7_mail_sent_function' );

	function monopoly_wpcf7_mail_sent_function( $contact_form ) {
		// Перехватываем данные из Contact Form 7
		$title = $contact_form->title;
		$posted_data = $contact_form->posted_data;

		// Формируем URL
		$idUser = 89;
		$codeWebhooks = 'ps6uexynz0naqeqj';
		$queryUrl = 'https://monopolyfranch.bitrix24.ru/rest/'.$idUser.'/'.$codeWebhooks.'/crm.lead.add.json';

		$queryData = '';

		if ('Contact form 1' == $title ) {
			$submission = WPCF7_Submission::get_instance();
			$posted_data = $submission->get_posted_data();

			// Перехватываем введенные данные в полях Contact Form 7
			$firstName = $posted_data['your-name'];
			$phone = $posted_data['your-phone']; 
			$city = $posted_data['your-city'];
			$birthDate = str_replace('-', '.', $posted_data['your-birth_date']); 

			// Формируем параметры для создания лида в переменной $queryData
			$queryData = http_build_query(array(
				'fields' => array(
					// Устанавливаем название для заголовка лида
					'TITLE' => 'Лид с сайта - https://job.monopoly.su',
					'NAME' => $firstName,
					'PHONE' => Array(
						"n0" => Array(
							"VALUE" => $phone,
							"VALUE_TYPE" => "WORK",
					    ),
					),
					'UF_CRM_1599323761' => $city,
					'UF_CRM_1607594001308' => $birthDate,
					'ASSIGNED_BY_ID' => 225,
					'SOURCE_ID' => 'WEB',
					'UTM_SOURCE' => isset($posted_data['utm_source']) ? $posted_data['utm_source'] : '',
					'UTM_MEDIUM' => isset($posted_data['utm_medium']) ? $posted_data['utm_medium'] : '',
					'UTM_CAMPAIGN' => isset($posted_data['utm_campaign']) ? $posted_data['utm_campaign'] : '',
					'UTM_CONTENT' => isset($posted_data['utm_content']) ? $posted_data['utm_content'] : '',
					'UTM_TERM' => isset($posted_data['utm_term']) ? $posted_data['utm_term'] : '',
				),
				'params' => array("REGISTER_SONET_EVENT" => "Y")
			));
		}

		if($queryData !== '') {
			// Обращаемся к Битрикс24 при помощи функции curl_exec
			$curl = curl_init();
			curl_setopt_array($curl, array(
				CURLOPT_SSL_VERIFYPEER => 0,
				CURLOPT_POST => 1,
				CURLOPT_HEADER => 0,
				CURLOPT_RETURNTRANSFER => 1,
				CURLOPT_URL => $queryUrl,
				CURLOPT_POSTFIELDS => $queryData,
		    ));

			$result = curl_exec($curl);
			curl_close($curl);
			$result = json_decode($result, 1);

			if (array_key_exists('error', $result)) echo "Ошибка при сохранении лида: ".$result['error_description']."<br/>";
		}
	}

	function my_wpcf7_dropdown_form($html) {
		if (stristr($html, 'select-citizenship')) {
			$text = 'Гражданство';
			$html = str_replace('---', '' . $text . '', $html);
			$html = str_replace('<option value="">Гражданство', '<option value="" disabled selected>Гражданство', $html);
		}
		return $html;
	}
	add_filter('wpcf7_form_elements', 'my_wpcf7_dropdown_form');