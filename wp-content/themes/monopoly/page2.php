<?php get_header(); ?>
	<div class="page-content">
		<div class="content">
			<?php while ( have_posts() ) : the_post(); ?>
				
				<?php the_title( '<h1 class="my-4 page-title">', '</h1>' ); ?>

				<?php the_content(); ?>

			<?php endwhile; ?>
		</div>
	</div>
<?php get_footer(); ?>