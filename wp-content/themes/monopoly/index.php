<?php get_header(); ?>
   
   <div class="main-screen">
      <div class="main-screen__media">
         <div class="main-screen__img">
            <img src="<?= $redux_demo['homeOneblock__image']['url'] ?>" alt="">
         </div>
      </div>
      <div class="main-screen__content content">
         <div class="main-screen__info">
            <div class="main-screen__name">
               <?= $redux_demo['homeOneblock__title']; ?>
            </div>
         </div>
      </div>
   </div>

   <div class="advWork-section" id="workWithUs">
      <div class="content">
         <div class="box-style">
            <div class="box-style__header">
               <div class="box-style__heading">
                  <?= $redux_demo['hometwoblock__title']; ?>
               </div>
            </div>
         </div>
         <div class="advWork-section__body">
            <?= $redux_demo['hometwoblock__adv']; ?>
         </div>
         <div class="advWork-section__map">
            <img src="<?= $redux_demo['hometwoblock__image']['url'] ?>" alt="">
         </div>
         <?= $redux_demo['hometwoblock__but']; ?>
      </div>
   </div>

   <div class="advantages-section">
      <div class="content">
         <?= $redux_demo['homeadvantages__desc']; ?>
      </div>
   </div>

   <div class="advWorkMust-section" id="workStart">
      <div class="content">
         <div class="box-style">
            <div class="box-style__header">
               <div class="box-style__heading">
                  <?= $redux_demo['homefourblock__title']; ?>
               </div>
            </div>
         </div>
         <div class="advWorkMust-section__body">
            <?= $redux_demo['homefourblock__adv']; ?>
         </div>
         <div class="advWorkMust-section__footer fl fl-wr-w fl-ju-co-sp-b">
            <div class="advWorkMust-section__but">
               <?= $redux_demo['homefourblock__but']; ?>
            </div>
            <div class="advWorkMust-section__image">
               <img src="<?= $redux_demo['homefourblock__image']['url'] ?>" alt="">
            </div>
         </div>
      </div>
   </div>

   <div class="advWorkFor-section" id="workFor">
      <div class="content fl fl-wr-w">
         <div class="advWorkFor-section__info">
            <div class="box-style">
               <div class="box-style__header">
                  <div class="box-style__heading">
                     <?= $redux_demo['homethreeblock__title']; ?>
                  </div>
               </div>
            </div>
            <?= $redux_demo['homethreeblock__adv']; ?>
            <?= $redux_demo['homethreeblock__but']; ?>
         </div>
         <div class="advWorkFor-section__image">
            <img src="<?= $redux_demo['homethreeblock__image']['url'] ?>" alt="">
         </div>
      </div>
   </div>

   <div class="advWorkMust-section">
      <div class="content">
         <div class="footer fl fl-wr-w fl-ju-co-sp-b">
            <div class="footer__item footer__item_1 fl fl-di-c fl-ju-co-sp-b">
               <div class="footer__logo">
                  <img src="<?= $redux_demo['redux-footer-logo']['url'] ?>" alt="">
               </div>
               <div class="footer__copy">
                  <?= $redux_demo['redux-footer-copyright']; ?>
               </div>
            </div>
            <div class="footer__item footer__item_2">
               <div class="footer-heading">Контакты</div>
               <div class="footer__contact footer-contact">
                     <div class="footer-contact__item footer-contact__phone">
                        <?= $redux_demo['contacts__phone']; ?>
                     </div>
                     <div class="footer-contact__item footer-contact__email">
                        <?= $redux_demo['contacts__email']; ?>
                     </div>
                     <div class="footer-contact__item footer-contact__location">
                        <?= $redux_demo['contacts__location']; ?>
                     </div>
                  </div>
               <?php wp_nav_menu(['menu' => 'footer']);?>
               <?= $redux_demo['redux-footer-oferta']; ?>
            </div>
            <div class="footer__item footer__item_3">
               <?= $redux_demo['redux-footer-termins']; ?>
            </div>
         </div>
      </div>
   </div>

<?php get_footer(); ?>