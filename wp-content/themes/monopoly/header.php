<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="">
    <meta property="og:locale" content="ru_RU" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Работа водителем категории Е в крупной компании с ЗП от 90 000- 120 000 руб!"/>
    <meta property="og:image" content="https://job.monopoly.su/wp-content/uploads/2020/10/main-screen.jpg" />

    <meta property="telegram:title" content="Работа водителем категории Е в крупной компании с ЗП от 90 000- 120 000 руб!"/>
    <meta property="telegram:image" content="https://job.monopoly.su/wp-content/uploads/2020/10/main-screen.jpg" />
    <meta name="yandex-verification" content="17cf87df8fde476d" />
    <meta name="google-site-verification" content="mi7_SsqCD3AAb28botX0jjyoA5gT0POYiUStBuRyvrw" />
    <?php wp_head();?>
    <?php global $redux_demo; ?>
    <?php /*$redux_demo['redux-header-script'];*/ ?>
</head>
<body>
    <div class="wrapper">
        <div class="wrap1">
            <header>
                <div class="header">
                    <div class="content fl fl-wr-w fl-al-it-c fl-ju-co-sp-b">
                        <div class="header__logo">
                            <?php the_custom_logo(); ?>
                        </div>
                        <div class="header__section header-section fl fl-al-it-c fl-ju-co-sp-b">
                            <div class="header-section__menu">
                                <?php wp_nav_menu(['menu' => 'top']);?>
                            </div>
                            <div class="header-section__contact header-contact">
                                <div class="header-contact__phone">
                                    <?= $redux_demo['contacts__phone']; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content">
                    <div class="logo-text">
                        <?= $redux_demo['logo_text']; ?>
                        <a class="logo-text__link" data-toggle="modal" data-target="#contactForm" href="/">Узнать подробнее</a>
                    </div>
                </div>
            </header>
