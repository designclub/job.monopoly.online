<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
// define( 'DB_NAME', 'u0707261_monopol' );
define( 'DB_NAME', 'u0707261_monopoly_work' );

/** Имя пользователя MySQL */
define( 'DB_USER', 'root' );

/** Пароль к базе данных MySQL */
define( 'DB_PASSWORD', '' );

/** Имя сервера MySQL */
define( 'DB_HOST', '127.0.0.1' );

/** Кодировка базы данных для создания таблиц. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Схема сопоставления. Не меняйте, если не уверены. */
define( 'DB_COLLATE', '' );

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Cj#;NoIp3O]k0{CgB5xq;<k_59IEE9.kXUkPY~gVLC^NUld-ouI?k[!Vl4nZ )2 ' );
define( 'SECURE_AUTH_KEY',  'f~4p^h+UhQnLWy<_Ui5K>#`tA8i$Fo,1M)+RGl9ct&`trY46|4t`B?9Q*/ILzXe_' );
define( 'LOGGED_IN_KEY',    'RV>$]XDKD86!^sR-O?8HLL3CXI|b{S2!I;/:a@ZLK|)+,U90G<#B|hOeZN/ m ~x' );
define( 'NONCE_KEY',        'zvFM.S<]S 56#{YJ{Y`Gd&H3UUy]m|+&1~DXuSa<`&).x#h.mNUqwI<Ni.iT[V|Q' );
define( 'AUTH_SALT',        's%*Y3PfE8[z*.>JdA{[g23R^2a6Im4<_G}I.2<3TvJ0B8mCPJh5PjuP&ZLDLE5t5' );
define( 'SECURE_AUTH_SALT', '~%fy]U/m`Z[,C{ec=*2[YfFg7CrF<az9ih6`+VM=?6d!?L-BxGO:sD-Jr&B46hl(' );
define( 'LOGGED_IN_SALT',   '$wDnkG4}amP%,0!(Y#Is0?7u{u]eao5c;k|yp02SymLr{-=h2faQu0}!eIofPk5x' );
define( 'NONCE_SALT',       'oegC<u/mvKBM8Nk4Dr-| $wH*P;~J.[iB)%<Ot~D4_s2gcBV4/,y1Ew2~>%0a~|{' );

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', true );

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Инициализирует переменные WordPress и подключает файлы. */
require_once( ABSPATH . 'wp-settings.php' );
